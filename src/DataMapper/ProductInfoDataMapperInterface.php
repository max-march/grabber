<?php declare(strict_types=1);

namespace App\DataMapper;

use App\DataTransferObject\DTOInterface\ProductParsedDataDTOInterface;

/**
 * Interface ProductInfoDataMapperInterface
 * @package App\DataMapper
 */
interface ProductInfoDataMapperInterface
{
    public function setData(
        string $vendor,
        ?string $vendorCode,
        ?string $description,
        ?string $keyWords,
        ?string $positionName,
        ?int $price,
        ?string $images,
        ?int $uniqueId
    ): ProductParsedDataDTOInterface;

    /**
     * @param ProductParsedDataDTOInterface $entity
     * @return array
     */
    public function toArray(ProductParsedDataDTOInterface $entity): array;
}
