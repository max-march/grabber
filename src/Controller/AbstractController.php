<?php declare(strict_types=1);

namespace App\Controller;

use App\Factory\JsonResponseFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractController
 * @package App\Controller
 */
abstract class AbstractController extends BaseController
{
    public const NOT_FOUND_RESPONSE_ARRAY = [
        self::RESPONSE_KEY_SUCCESS => false,
        self::RESPONSE_KEY_STATUS => Response::HTTP_NOT_FOUND
    ];

    public const SUN_SITE_PARAM = 'sun_site';
    public const DIASHA_SITE_PARAM = 'diasha_site';
    public const TRUE = 'true';
    public const FALSE = 'false';

    protected const RESPONSE_KEY_ERRORS = 'errors';
    protected const CATEGORIES_PARAM = 'categories';
    protected const REQUEST_KEY_CATEGORY = 'category';
    protected const PRODUCTS_PARAM = 'products';

    protected const RESPONSE_KEY_STATUS = 'status';
    protected const RESPONSE_KEY_SUCCESS = 'success';
    protected const RESPONSE_KEY_PAYLOAD = 'payload';

    protected const CREATED_RESPONSE_ARRAY = [
        self::RESPONSE_KEY_SUCCESS => true,
        self::RESPONSE_KEY_STATUS => Response::HTTP_CREATED,
    ];

    protected const SUCCESS_RESPONSE_ARRAY = [
        self::RESPONSE_KEY_SUCCESS => true,
        self::RESPONSE_KEY_STATUS => Response::HTTP_OK,
    ];

    protected const RESPONSES = [
        Response::HTTP_CREATED => self::CREATED_RESPONSE_ARRAY,
        Response::HTTP_OK => self::SUCCESS_RESPONSE_ARRAY,
        Response::HTTP_NOT_FOUND => self::NOT_FOUND_RESPONSE_ARRAY
    ];

    /**
     * @var JsonResponseFactory $jsonResponseFactory
     */
    private $jsonResponseFactory;

    /**
     * ResponseHelper constructor.
     * @param JsonResponseFactory $jsonResponseFactory
     */
    public function __construct(JsonResponseFactory $jsonResponseFactory)
    {
        $this->jsonResponseFactory = $jsonResponseFactory;
    }

    /**
     * @param string $value
     * @return bool
     */
    protected function stingToBoolean(string $value): bool
    {
        return $value === self::TRUE;
    }

    /**
     * @param int $code
     * @return JsonResponse|null
     */
    protected function sendResponse(int $code): ?JsonResponse
    {
        if (array_key_exists($code, self::RESPONSES)) {
            return  $this->jsonResponseFactory->create(self::RESPONSES[$code], $code);
        }

        return null;
    }

    /**
     * @param array $data
     * @return JsonResponse
     */
    protected function sendResponseWithData(array $data): JsonResponse
    {
        return  $this->jsonResponseFactory->create(
            array_merge(self::SUCCESS_RESPONSE_ARRAY, [self::RESPONSE_KEY_PAYLOAD => $data]),
            Response::HTTP_OK
        );
    }
}
