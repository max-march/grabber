<?php declare(strict_types=1);

namespace App\Controller;

use App\Factory\JsonResponseFactory;
use App\Manager\CategoryManager;
use App\Manager\ProductManager;
use App\Parser\SunProductParser;
use App\Queues\Producer;
use App\Queues\QueuesInterface\ProducerInterface;
use Interop\Queue\Exception;
use Interop\Queue\Exception\Exception as ProducerException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SunProductController
 * @package App\Controller
 */
class SunProductController extends AbstractController
{
    /**
     * @var ProductManager $manager
     */
    private $manager;

    /**
     * @var CategoryManager $categoryManager
     */
    private $categoryManager;

    /**
     * @var SunProductParser $parser
     */
    private $parser;

    /**
     * @var Producer $producer
     */
    private $producer;

    /**
     * SunProductController constructor.
     * @param JsonResponseFactory $jsonResponseFactory
     * @param ProductManager $manager
     * @param CategoryManager $categoryManager
     * @param SunProductParser $parser
     * @param ProducerInterface $producer
     */
    public function __construct(
        JsonResponseFactory $jsonResponseFactory,
        ProductManager $manager,
        CategoryManager $categoryManager,
        SunProductParser $parser,
        ProducerInterface $producer
    ) {
        parent::__construct($jsonResponseFactory);
        $this->manager = $manager;
        $this->categoryManager = $categoryManager;
        $this->parser = $parser;
        $this->producer = $producer;
    }

    /**
     * @param ParameterBagInterface $params
     * @return Response
     */
    public function list(ParameterBagInterface $params): Response
    {
        $products = $this->manager->getAllBySite($params->get(self::SUN_SITE_PARAM));
        return $this->render('sun-line/products.html.twig', [self::PRODUCTS_PARAM => $products]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function sync(Request $request): JsonResponse
    {
        $categoryIds = $request->get(self::CATEGORIES_PARAM);
        $categories = $this->categoryManager->getByIds($categoryIds);
        $this->manager->removeAll();
        $products = $this->parser->parseAllByCategories($categories);
        $this->manager->create($products);

        return $this->sendResponse(Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     * @throws ProducerException
     */
    public function generateFile(Request $request): JsonResponse
    {
        try {
            $category = $request->get(self::REQUEST_KEY_CATEGORY);
            $this->producer->addTask($category, Producer::QUEUE_SUN_NAME);

            return $this->sendResponse(Response::HTTP_OK);
        } catch (ProducerException $exception) {
            throw new ProducerException($exception->getMessage(), $exception->getCode());
        }
    }
}
