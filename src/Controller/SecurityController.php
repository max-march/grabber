<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package App\Controller
 */
class SecurityController extends AbstractController
{
    private const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @param AuthenticationUtils $authUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authUtils): ?Response
    {
        if (!$this->isGranted(self::ROLE_ADMIN)) {
            return $this->render('login.html.twig', [
                'last_username' => $authUtils->getLastUsername(),
                'error' => $authUtils->getLastAuthenticationError(),
            ]);
        }

        return $this->redirect('/admin/dashboard');
    }
}
