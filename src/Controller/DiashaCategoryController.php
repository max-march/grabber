<?php declare(strict_types=1);

namespace App\Controller;

use App\Factory\JsonResponseFactory;
use App\Manager\CategoryManager;
use App\Parser\DiashaCategoryParser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DiashaCategoryController
 * @package App\Controller
 */
class DiashaCategoryController extends AbstractController
{
    private const REQUEST_KEY_START_PAGE_NUMBER = 'startPageValue';
    private const REQUEST_KEY_LAST_PAGE_NUMBER = 'lastPageValue';

    /**
     * @var CategoryManager $manager
     */
    private $manager;

    /**
     * DiashaCategoryController constructor.
     * @param JsonResponseFactory $jsonResponseFactory
     * @param CategoryManager $manager
     */
    public function __construct(
        JsonResponseFactory $jsonResponseFactory,
        CategoryManager $manager
    ) {
        parent::__construct($jsonResponseFactory);
        $this->manager = $manager;
    }

    /**
     * @param ParameterBagInterface $params
     * @return Response
     */
    public function list(ParameterBagInterface $params): Response
    {
        $categories = $this->manager->getAllBySite($params->get(self::DIASHA_SITE_PARAM));
        return $this->render('diasha/categories.html.twig', [self::CATEGORIES_PARAM => $categories]);
    }

    /**
     * @param DiashaCategoryParser $parser
     * @return JsonResponse
     */
    public function sync(DiashaCategoryParser $parser): JsonResponse
    {
        $categories = $parser->parseAll();
        $this->manager->create($categories);

        return $this->sendResponse(Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function pageNumbers(Request $request, int $id): JsonResponse
    {
        $startPageNumber = (int)$request->get(self::REQUEST_KEY_START_PAGE_NUMBER);
        $lastPageNumber = (int)$request->get(self::REQUEST_KEY_LAST_PAGE_NUMBER);

        $this->manager->refreshPageNumbers($startPageNumber, $lastPageNumber, $id);

        return $this->sendResponse(Response::HTTP_OK);
    }
}
