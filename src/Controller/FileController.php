<?php declare(strict_types=1);

namespace App\Controller;

use App\Factory\JsonResponseFactory;
use App\Manager\FileManager;
use App\Service\FileService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FileController
 * @package App\Controller
 */
class FileController extends AbstractController
{
    private const FILE_PARAM = 'fileName';
    private const FILES_PARAM = 'files';
    private const FILE_NOT_EXIST_ERROR = 'The file does not exist';

    /**
     * @var FileService $service
     */
    private $service;

    /**
     * @var FileManager $manager
     */
    private $manager;

    /**
     * FileController constructor.
     * @param FileService $downloadFileService
     * @param JsonResponseFactory $jsonResponseFactory
     * @param FileManager $manager
     */
    public function __construct(
        FileService $downloadFileService,
        JsonResponseFactory $jsonResponseFactory,
        FileManager $manager
    ) {
        parent::__construct($jsonResponseFactory);
        $this->service = $downloadFileService;
        $this->manager = $manager;
    }

    /**
     * @param Request $request
     * @return JsonResponse|null
     * @throws Exception
     */
    public function getFile(Request $request): ?JsonResponse
    {
        $file = $request->get(self::FILE_PARAM);
        if ($this->service->downloadFile($file) !== null) {
            return $this->sendResponse(Response::HTTP_OK);
        }

        throw $this->createNotFoundException(self::FILE_NOT_EXIST_ERROR);
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('files/index.html.twig', [
            self::FILES_PARAM => $this->manager->getAll()
        ]);
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(int $id): JsonResponse
    {
        $this->manager->remove($id);
        return $this->sendResponse(Response::HTTP_OK);
    }
}
