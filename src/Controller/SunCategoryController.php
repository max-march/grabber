<?php declare(strict_types=1);

namespace App\Controller;

use App\Factory\JsonResponseFactory;
use App\Manager\CategoryManager;
use App\Parser\SunCategoryParser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SunCategoryController
 * @package App\Controller
 */
class SunCategoryController extends AbstractController
{
    private const REQUEST_KEY_PAGINATION = 'pagination';

    /**
     * @var CategoryManager $manager
     */
    private $manager;

    /**
     * SunCategoryController constructor.
     * @param JsonResponseFactory $jsonResponseFactory
     * @param CategoryManager $manager
     */
    public function __construct(
        JsonResponseFactory $jsonResponseFactory,
        CategoryManager $manager
    ) {
        parent::__construct($jsonResponseFactory);
        $this->manager = $manager;
    }

    /**
     * @param ParameterBagInterface $params
     * @return Response
     */
    public function list(ParameterBagInterface $params): Response
    {
        $categories = $this->manager->getAllBySite($params->get(self::SUN_SITE_PARAM));
        return $this->render('sun-line/categories.html.twig', [self::CATEGORIES_PARAM => $categories]);
    }

    /**
     * @param SunCategoryParser $parser
     * @return JsonResponse
     */
    public function sync(SunCategoryParser $parser): JsonResponse
    {
        $categories = $parser->parseAll();
        $this->manager->create($categories);

        return $this->sendResponse(Response::HTTP_OK);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function setPagination(int $id, Request $request): JsonResponse
    {
        $pagination = (int)$request->get(self::REQUEST_KEY_PAGINATION);
        $this->manager->refreshPagination($pagination, $id);

        return $this->sendResponse(Response::HTTP_OK);
    }
}
