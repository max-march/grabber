<?php declare(strict_types=1);

namespace App\Handlers;

use App\Controller\AbstractController;
use App\Factory\JsonResponseFactory;
use App\Manager\ProductManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DeleteProductHandler
 * @package App\Handlers
 */
class DeleteProductHandler extends AbstractController
{
    /**
     * @var ProductManager $manager
     */
    private $manager;

    public function __construct(
        JsonResponseFactory $jsonResponseFactory,
        ProductManager $manager
    ) {
        parent::__construct($jsonResponseFactory);
        $this->manager = $manager;
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function run(int $id): JsonResponse
    {
        $this->manager->remove($id);
        return $this->sendResponse(Response::HTTP_OK);
    }
}
