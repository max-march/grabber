<?php declare(strict_types=1);

namespace App\Queues;

use App\Queues\QueuesInterface\ProducerInterface;
use Enqueue\Dbal\DbalConnectionFactory;
use Interop\Queue\Exception;
use Interop\Queue\Exception\InvalidDestinationException;
use Interop\Queue\Exception\InvalidMessageException;

/**
 * Class Producer
 */
class Producer implements ProducerInterface
{
    public const QUEUE_SUN_NAME = 'sun-product-report';
    public const QUEUE_DIASHA_NAME = 'diasha-product-report';

    /**
     * @var DbalConnectionFactory $context
     */
    private $context;

    /**
     * Producer constructor.
     * @param DbalConnectionFactory $context
     */
    public function __construct(DbalConnectionFactory $context)
    {
        $this->context = $context->createContext();
    }

    /**
     * @param string $category
     * @param string $queueName
     * @throws Exception
     * @throws Exception\Exception
     * @throws InvalidDestinationException
     * @throws InvalidMessageException
     */
    public function addTask(string $category, string $queueName): void
    {
        $this->context->createDataBaseTable();
        $queue = $this->context->createQueue($queueName);
        $message = $this->context->createMessage($category);

        $this->context->createProducer()->send($queue, $message);
    }
}
