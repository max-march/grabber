<?php declare(strict_types=1);

namespace App\Queues\QueuesInterface;

/**
 * Interface ProducerInterface
 * @package App\Queues\QueuesInterface
 */
interface ProducerInterface
{
    /**
     * @param string $category
     * @param string $queueName
     */
    public function addTask(string $category, string $queueName): void;
}
