<?php declare(strict_types=1);

namespace App\Queues\QueuesInterface;

/**
 * Interface ConsumerInterface
 * @package App\Queues
 */
interface ConsumerInterface
{
    public function startProcessing(string $site): void;
}
