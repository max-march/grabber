<?php declare(strict_types=1);

namespace App\Queues;

use Twig\Error\LoaderError;
use Twig\Error\SyntaxError;
use Twig\Error\RuntimeError;
use App\Manager\FileManager;
use App\Service\EmailService;
use Doctrine\ORM\ORMException;
use App\Service\ProductService;
use App\Manager\CategoryManager;
use Enqueue\Dbal\DbalDestination;
use PHPMailer\PHPMailer\Exception;
use App\Parser\ProductParserInterface;
use Enqueue\Dbal\DbalConnectionFactory;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\NonUniqueResultException;
use App\Exception\ZipFileGeneratorException;
use App\Queues\QueuesInterface\DiashaConsumerInterface;

/**
 * Class DiashaConsumer
 * @package App\Queues
 */
class DiashaConsumer implements DiashaConsumerInterface
{
    /**
     * @var DbalConnectionFactory $context
     */
    private $context;

    /**
     * @var DbalDestination $dbalDestination
     */
    private $dbalDestination;

    /**
     * @var ProductService $service
     */
    private $service;

    /**
     * @var EmailService $emailService
     */
    private $emailService;

    /**
     * @var FileManager $fileManager
     */
    private $fileManager;

    /**
     * @var CategoryManager $categoryManager
     */
    private $categoryManager;

    /**
     * @var ProductParserInterface $parser
     */
    private $parser;

    /**
     * SunConsumer constructor.
     * @param DbalConnectionFactory $context
     * @param DbalDestination $dbalDestination
     * @param ProductService $service
     * @param EmailService $emailService
     * @param FileManager $fileManager
     * @param CategoryManager $categoryManager
     * @param ProductParserInterface $parser
     */
    public function __construct(
        DbalConnectionFactory $context,
        DbalDestination $dbalDestination,
        ProductService $service,
        EmailService $emailService,
        FileManager $fileManager,
        CategoryManager $categoryManager,
        ProductParserInterface $parser
    ) {
        $this->context = $context->createContext();
        $this->dbalDestination = $dbalDestination;
        $this->service = $service;
        $this->emailService = $emailService;
        $this->fileManager = $fileManager;
        $this->categoryManager = $categoryManager;
        $this->parser = $parser;
    }

    /**
     * @param string $site
     * @throws Exception
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws ZipFileGeneratorException
     * @throws NonUniqueResultException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function startProcessing(string $site): void
    {
        if ($this->service->getQueueCount(Producer::QUEUE_DIASHA_NAME) !== 0) {
            $consumer = $this->context->createConsumer($this->dbalDestination);
            $message = $consumer->receive();
            $consumer->acknowledge($message);

            $this->service->addParser($this->parser, $site);
            $this->service->setParser($site);
            $filePath = $this->service->generateFile();

            $category = $message->getBody();
            $this->emailService->send($filePath, EmailService::SUN_LINE_TEMPLATE_NAME, $category, $site);

            $category = $this->categoryManager->getByName($category);
            $this->fileManager->create($category, $filePath, basename($filePath));
        }
    }
}
