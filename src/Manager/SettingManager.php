<?php declare(strict_types=1);

/**
 * @author: Max Marchenko <max@heartpace.com>
 * @version:
 * Datetime: 29.10.2019 23:55
 */

namespace App\Manager;

use Doctrine\ORM\ORMException;
use App\Repository\SettingRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class SettingManager
 * @package App\Manager
 */
class SettingManager
{
    /**
     * @var SettingRepository
     */
    private $repository;

    /**
     * SettingManager constructor.
     * @param SettingRepository $repository
     */
    public function __construct(SettingRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param bool $emailSend
     * @throws ORMException
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function update(bool $emailSend): void
    {
        $settings = $this->repository->findFirst();
        if ($settings) {
            $settings->setEmailSend($emailSend);
            $this->repository->save($settings);
        }
    }

    /**
     * @return bool
     * @throws NonUniqueResultException
     */
    public function getEmailSendConfiguration(): bool
    {
        $settings = $this->repository->findFirst();
        if ($settings) {
            return $settings->getEmailSend();
        }

        return false;
    }
}
