<?php declare(strict_types=1);

namespace App\Helper;

use App\Kernel;

/**
 * Class KernelHelper
 * @package App\Helper
 */
class KernelHelper
{
    private const PROD = 'prod';
    private const PUBLIC_DIR_PATTERN = '%s/public';

    /**
     * @return string
     */
    public static function getPublicDir(): string
    {
        $projectDir = (new Kernel(self::PROD, false))->getProjectDir();
        return sprintf(self::PUBLIC_DIR_PATTERN, $projectDir);
    }
}
