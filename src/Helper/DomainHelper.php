<?php declare(strict_types=1);

namespace App\Helper;

/**
 * Class DomainHelper
 * @package App\Helper
 */
class DomainHelper
{
    private const HOST = 'host';

    /**
     * @param string $url
     * @return string
     */
    public static function getBaseUrl(string $url): string
    {
        return parse_url($url)[self::HOST];
    }
}
