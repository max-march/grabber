<?php declare(strict_types=1);

namespace App\Helper;

/**
 * Class LowerCasingEnvVarHelper
 * @package App\Helper
 */
class EnvVarHelper
{
    public const APP_ENV = 'APP_ENV';

    /**
     * @param string $name
     * @return string
     */
    public static function getEnv(string $name): string
    {
        return getenv($name);
    }
}
