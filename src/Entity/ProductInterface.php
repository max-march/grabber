<?php declare(strict_types=1);

namespace App\Entity;

/**
 * Interface ProductInterface
 * @package App\Entity
 */
interface ProductInterface extends EntityInterface
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @return string|null
     */
    public function getImage(): ?string;

    /**
     * @return string|null
     */
    public function getLink(): ?string;

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category;

    /**
     * @return string|null
     */
    public function getSiteName(): ?string;
}
