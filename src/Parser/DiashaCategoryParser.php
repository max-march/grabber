<?php declare(strict_types=1);

namespace App\Parser;

use Generator;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;
use App\Helper\DomainHelper;
use PHPHtmlParser\Dom\HtmlNode;
use PHPHtmlParser\Dom\Collection;
use App\Factory\CategoryDTOFactory;
use App\DataTransferObject\CategoryDTO;
use App\DataTransferObject\DTOInterface\CategoryDTOInterface;

/**
 * Class DiashaCategoryParser
 * @package App\ParsingRepository
 */
class DiashaCategoryParser extends AbstractParser
{
    private const SITE_LINK = 'https://diasha.com.ua';
    private const CATEGORY_HTML_PATTERN = 'main.content div.fullcard div.nav_cat div.nav_cat_item a';
    private const PAGINATION_HTML_PATTERN = 'div.pagination ul li.paginationItem a';

    /**
     * @var CategoryDTOFactory $categoryDTOFactory
     */
    private $categoryDTOFactory;

    /**
     * DiashaCategoryParser constructor.
     * @param Dom $dom
     * @param Client $httpClient
     * @param CategoryDTOFactory $categoryDTOFactory
     */
    public function __construct(Dom $dom, Client $httpClient, CategoryDTOFactory $categoryDTOFactory)
    {
        parent::__construct($dom, $httpClient);
        $this->categoryDTOFactory = $categoryDTOFactory;
    }

    /**
     * @return Generator
     */
    public function parseAll(): Generator
    {
        /** @var Collection $categoriesCollection */

        $html = $this->getHtml(self::SITE_LINK);
        $html = str_replace('</a></a>', '</a>', $html);

        $this->domParser->load($html);
        $categoriesCollection = $this->domParser->find(self::CATEGORY_HTML_PATTERN);

        $categories = [];

        foreach ($categoriesCollection as $value) {
            $category = $category = $this->category($value);

            if ($category) {
                $categories[] = $category;
            }
        }

        foreach ($categories as $category) {
            yield $this->addPaginationData($category);
        }
    }

    /**
     * @param HtmlNode $htmlNode
     * @return CategoryDTOInterface|null
     */
    private function category(HtmlNode $htmlNode): ?CategoryDTOInterface
    {
        /** @var Collection $categoryNameCollection */
        $categoryNameCollection = $htmlNode->find(self::H2);

        /** @var HtmlNode $categoryNode */
        $categoryNode = $categoryNameCollection->offsetGet(self::FIRST_ELEMENT);
        $categoryName = $categoryNode->text();
        $link = $htmlNode->getAttribute(self::HREF_ATTRIBUTE);

        if ($categoryName) {
            $siteLink = DomainHelper::getBaseUrl(self::SITE_LINK);
            $fullLink = $link ? self::SITE_LINK . $link : null;

            return $this->categoryDTOFactory->create($categoryName, $fullLink, $siteLink);
        }

        return null;
    }

    /**
     * @param CategoryDTO $category
     * @return CategoryDTO
     */
    private function addPaginationData(CategoryDTO $category): CategoryDTO
    {
        $this->domParser->loadFromUrl($category->getLink());
        /** @var Collection $paginationCollection */
        $paginationCollection = $this->domParser->find(self::PAGINATION_HTML_PATTERN);
        $elementIndex = $paginationCollection->count() - 3;

        /** @var HtmlNode $element */
        $element = $paginationCollection->offsetGet($elementIndex);
        $paginationCount = $element->text();
        $paginationPattern = $element->getAttribute(self::HREF_ATTRIBUTE);
        $paginationPattern = str_replace($paginationCount, '%s', $paginationPattern);

        return $category->setPagination((int)$paginationCount)->setLinkPattern($paginationPattern);
    }
}
