<?php declare(strict_types=1);

namespace App\Parser;

use App\Factory\CategoryDTOFactory;
use App\Helper\DomainHelper;
use Generator;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;

/**
 * Class SunCategoryParser
 */
class SunCategoryParser extends AbstractParser
{
    private const SITE_LINK = 'http://linisoln.com.ua/';
    private const MAIN_CATEGORY_HTML_PATTERN = 'div#content-category ul li ul li a';

    /**
     * @var CategoryDTOFactory $categoryDTOFactory
     */
    private $categoryDTOFactory;

    /**
     * SunCategoryParser constructor.
     * @param Dom $dom
     * @param Client $httpClient
     * @param CategoryDTOFactory $categoryDTOFactory
     */
    public function __construct(Dom $dom, Client $httpClient, CategoryDTOFactory $categoryDTOFactory)
    {
        parent::__construct($dom, $httpClient);
        $this->categoryDTOFactory = $categoryDTOFactory;
    }

    /**
     * @return Generator
     */
    public function parseAll(): Generator
    {
        $document = $this->getHtml(self::SITE_LINK);
        $this->domParser->load($document);
        $categoryLinks = $this->domParser->find(self::MAIN_CATEGORY_HTML_PATTERN);

        foreach ($categoryLinks as $link) {
            /** @var HtmlNode $link */
            $categoryLink = $link->getAttribute(self::HREF_ATTRIBUTE);

            yield $this->categoryDTOFactory->create(
                $link->text(),
                $categoryLink,
                DomainHelper::getBaseUrl($categoryLink)
            );
        }
    }
}
