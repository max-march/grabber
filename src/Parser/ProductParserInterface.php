<?php declare(strict_types=1);

namespace App\Parser;

use App\Entity\ProductInterface;
use Generator;

/**
 * Interface ProductPurserInterface
 * @package App\Parser
 */
interface ProductParserInterface
{
    public function findInnerData(ProductInterface $product): Generator;
}
