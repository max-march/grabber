<?php declare(strict_types=1);

namespace App\Parser;

use App\DataMapper\ProductInfoDataMapper;
use App\DataMapper\ProductInfoDataMapperInterface;
use App\DataTransferObject\DTOInterface\ProductDTOInterface;
use App\Entity\Category;
use App\Entity\ProductInterface;
use App\Factory\ProductDTOFactory;
use App\Helper\StringHelper;
use Generator;
use GuzzleHttp\Client;
use PHPHtmlParser\Dom;
use PHPHtmlParser\Dom\HtmlNode;
use PHPHtmlParser\Dom\Collection;
use PHPHtmlParser\Exceptions\UnknownChildTypeException;

/**
 * Class DiashaProductParser
 * @package App\Parser
 */
class DiashaProductParser extends AbstractParser implements ProductParserInterface
{
    private const PRODUCT_HTML_PATTERN = 'main.content ul.cat-flex li.card';
    private const MAIN_DATA_PARAMS_HTML_PATTERN = 'div.tovar-block div.tovOpis div.tovRight div.tovParams';
    private const MAIN_DATA_DESCRIPTION_HTML_PATTERN = 'div.tovar-block div.tovOpis p';
    private const VENDOR_CODE_HTML_PATTERN =
        'div.tovar-block div.tovOpis div.tovRight div div.tovModel form div.form_radio label';
    private const POSITION_NAME_HTML_PATTERN = 'div.tovar-block h1';
    private const PRICE_HTML_PATTERN = 'div.tovar-block div.tovOpis div.tovRight div div.tovCena';
    private const IMAGES_HTML_PATTERN = 'div.tovar-block div.tovOpis div.tovPhoto img';

    private const CHECK_AVAILABILITY_BY_PHONE = 'Наличие товара уточняйте по телефону';
    private const NOT_AVAILABLE = 'Нет в наличии';

    private const NOT_AVAILABLE_PHRASES = [
        self::CHECK_AVAILABILITY_BY_PHONE,
        self::NOT_AVAILABLE,
    ];


    /**
     * @var ProductDTOFactory $factory
     */
    private $factory;

    /**
     * @var ProductInfoDataMapper $productDataMapper
     */
    private $productDataMapper;

    /**
     * SunProductParser constructor.
     * @param Dom $dom
     * @param Client $httpClient
     * @param ProductDTOFactory $factory
     * @param ProductInfoDataMapperInterface $productDataMapper
     */
    public function __construct(
        Dom $dom,
        Client $httpClient,
        ProductDTOFactory $factory,
        ProductInfoDataMapperInterface $productDataMapper
    ) {
        parent::__construct($dom, $httpClient);
        $this->factory = $factory;
        $this->productDataMapper = $productDataMapper;
    }

    /**
     * @param Category $category
     * @return ProductDTOInterface[]
     */
    protected function parse(Category $category): array
    {
        $products = [];

        $startPageNumber = $category->getStartPageNumber() ?: 1;
        $lastPageNumber = $category->getLastPageNumber() ?: $category->getPagination();

        for ($i = $startPageNumber; $i <= $lastPageNumber; $i++) {
            $document = $this->getHtml(sprintf($category->getLinkPattern(), $i));
            $this->domParser->load($document);

            /** @var Collection $productsCollection */
            $productsCollection = $this->domParser->find(self::PRODUCT_HTML_PATTERN);

            /** @var HtmlNode $value */
            foreach ($productsCollection as $value) {
                $link = $this->getDataFromNode($value, self::LINK, self::HREF_ATTRIBUTE);
                $image = $this->getDataFromNode($value, self::IMG, self::SRC_ATTRIBUTE);
                $name = $this->getDataFromNode($value, self::H2);

                $products[] = $this->factory->create($name, $link, $image, $category);
            }
        }

        return $products;
    }

    /**
     * @param HtmlNode $value
     * @param string $selector
     * @param string|null $attribute
     * @return string|null
     */
    private function getDataFromNode(HtmlNode $value, string $selector, ?string $attribute = null): ?string
    {
        /** @var Collection $collection */
        $collection = $value->find($selector);
        /** @var HtmlNode $node */
        $node = $collection->offsetGet(self::FIRST_ELEMENT);

        return $attribute ? $node->getAttribute($attribute) : $node->text();
    }

    /**
     * @param ProductInterface $product
     * @return Generator
     * @throws UnknownChildTypeException
     */
    public function findInnerData(ProductInterface $product): Generator
    {
        $this->domParser->loadFromUrl($product->getLink());
        $mainData = $this->findMainData($this->domParser);
        $vendorCodes = $this->findVendorCode($this->domParser);

        if ($vendorCodes) {
            foreach ($vendorCodes as $vendorCode) {
                yield $this->productDataMapper->setData(
                    ProductInfoDataMapper::COLUMN_VENDOR_DIASHA_VALUE,
                    $vendorCode,
                    implode(self::DESCRIPTION_GLUE, $mainData),
                    null,
                    $this->findPositionName($this->domParser),
                    $this->findPrice($this->domParser),
                    $this->findImages($this->domParser),
                    mt_rand()
                );
            }
        }
    }

    /**
     * @param Dom $domParser
     * @return array[]
     * @throws UnknownChildTypeException
     */
    private function findMainData(Dom $domParser): array
    {
        /** @var Collection $paramsCollection */
        $paramsCollection = $domParser->find(self::MAIN_DATA_PARAMS_HTML_PATTERN);

        /** @var HtmlNode $params */
        $params = $paramsCollection->offsetGet(self::FIRST_ELEMENT);
        $mainData = [];

        if ($params) {
            $params = $params->innerHtml();
            $mainData = $params ? explode('<br />', $params) : [];
        }

        /** @var Collection $descriptionCollection */
        $descriptionCollection = $domParser->find(self::MAIN_DATA_DESCRIPTION_HTML_PATTERN);
        $description = $descriptionCollection->offsetGet(self::FIRST_ELEMENT);

        return array_merge($mainData, [$description]);
    }

    /**
     * @param Dom $domParser
     * @return array
     */
    private function findVendorCode(Dom $domParser): array
    {
        $vendorsCodeCollection = $domParser->find(self::VENDOR_CODE_HTML_PATTERN);

        return $this->prepareVendorCode($vendorsCodeCollection);
    }

    /**
     * @param HtmlNode[]|Collection|<int, HtmlNode> $vendorsCodes
     * @return array
     */
    private function prepareVendorCode(Collection $vendorsCodes): array
    {
        $result = [];

        foreach ($vendorsCodes as $vendorNode) {
            $vendorCode = $vendorNode->text();
            $available = StringHelper::strposArray($vendorCode, self::NOT_AVAILABLE_PHRASES);

            if ($available) {
                continue;
            }

            $result[] = $vendorCode;
        }

        return $result;
    }

    /**
     * @param Dom $domParser
     * @return string
     */
    private function findPositionName(Dom $domParser): ?string
    {
        /** @var Collection $collection */
        $collection = $domParser->find(self::POSITION_NAME_HTML_PATTERN);

        /** @var HtmlNode $element */
        $element = $collection->offsetGet(self::FIRST_ELEMENT);

        return $element !== null ? $element->text() : null;
    }

    /**
     * @param Dom $domParser
     * @return int
     */
    private function findPrice(Dom $domParser): ?int
    {
        /** @var Collection $collection */
        $collection = $domParser->find(self::PRICE_HTML_PATTERN);
        $element = $collection->offsetGet(self::FIRST_ELEMENT);

        return $element !== null ? StringHelper::getNumber($element->text()) : null;
    }

    /**
     * @param Dom $domParser
     * @return string
     */
    private function findImages(Dom $domParser): string
    {
        $images = $domParser->find(self::IMAGES_HTML_PATTERN);

        $data = [];

        foreach ($images as $image) {
            /** @var HtmlNode $image */
            $data[] = $image->getAttribute(self::SRC_ATTRIBUTE);
        }

        return implode(self::DELIMITER, $data);
    }
}
