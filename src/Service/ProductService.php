<?php declare(strict_types=1);

namespace App\Service;

use Generator;
use Exception;
use App\Repository\ProductRepository;
use App\Parser\ProductParserInterface;
use App\DataMapper\ProductInfoDataMapper;
use App\Exception\ZipFileGeneratorException;
use App\DataMapper\ProductInfoDataMapperInterface;
use App\Repository\RepositoryInterface\CategoryRepositoryInterface;
use App\Repository\RepositoryInterface\RepositoryInterface;

/**
 * Class ProductService
 * @package App\Service
 */
class ProductService extends AbstractService
{
    /**
     * @var ProductRepository $repository
     */
    protected $repository;

    /**
     * @var CategoryRepositoryInterface $categoryRepository
     */
    protected $categoryRepository;

    /**
     * @var XlsxFileService $fileGenerator
     */
    private $fileGenerator;

    /**
     * @var ProductInfoDataMapper $productInfoDataMapper
     */
    private $productInfoDataMapper;

    /**
     * @var ZipFileService $zipGenerator
     */
    private $zipGenerator;

    /**
     * @var string $parserName
     */
    private $parserName;

    /**
     * @var array $parsers
     */
    private $parsers = [];

    /**
     * ProductService constructor.
     * @param ProductRepository $repository
     * @param CategoryRepositoryInterface $categoryRepository
     * @param XlsxFileService $fileGenerator
     * @param ProductInfoDataMapperInterface $productInfoDataMapper
     * @param ZipFileService $zipFileGenerator
     */
    public function __construct(
        ProductRepository $repository,
        CategoryRepositoryInterface $categoryRepository,
        XlsxFileService $fileGenerator,
        ProductInfoDataMapperInterface $productInfoDataMapper,
        ZipFileService $zipFileGenerator
    ) {
        parent::__construct($repository);
        $this->categoryRepository = $categoryRepository;
        $this->fileGenerator = $fileGenerator;
        $this->productInfoDataMapper = $productInfoDataMapper;
        $this->zipGenerator = $zipFileGenerator;
    }

    /**
     * @param ProductParserInterface $parser
     * @param string $parserName
     * @return void
     */
    public function addParser(ProductParserInterface $parser, string $parserName): void
    {
        $this->parsers[$parserName] = $parser;
    }

    /**
     * @param string $parser
     */
    public function setParser(string $parser): void
    {
        $this->parserName = $parser;
    }

    /**
     * @return string
     * @throws ZipFileGeneratorException
     * @throws Exception
     */
    public function generateFile(): string
    {
        $data = $this->prepareData();
        $writer = $this->fileGenerator->createFile(self::COLUMN);
        $this->chunkCsvData($data, $writer);
        $fileName = $this->generateFileName(XlsxFileService::REPORTS_FILE_PATH_PATTERN);
        $this->fileGenerator->saveFile($fileName);
        $zipFilePath = $this->generateFileName(ZipFileService::REPORTS_ZIP_FILE_PATH_PATTERN);
        $this->zipGenerator->generateZipArchive($fileName, $zipFilePath);
        unlink($fileName);

        return $zipFilePath;
    }

    /**
     * @return Generator
     */
    private function prepareData(): Generator
    {
        $products = $this->repository->findBy([RepositoryInterface::COLUMN_SITE => $this->parserName]);

        foreach ($products as $product) {
            $data = $this->parsers[$this->parserName]->findInnerData($product);

            foreach ($data as $datum) {
                yield $this->productInfoDataMapper->toArray($datum);
            }
        }
    }

    /**
     * @param Generator $data
     * @param int $limit
     * @return void
     */
    private function chunkCsvData(Generator $data, $limit = 1000): void
    {
        $count = 0;
        $arrayForCSVFile = [];
        foreach ($data as $datum) {
            $count++;
            $arrayForCSVFile[] = $datum;
            if ($count === $limit) {
                $this->fileGenerator->writeDataToFile($arrayForCSVFile);
                $count = 0;
                $arrayForCSVFile = [];
            }
        }
        if (!empty($arrayForCSVFile)) {
            $this->fileGenerator->writeDataToFile($arrayForCSVFile);
        }
    }
}
