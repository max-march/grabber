<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Category;
use App\Entity\EntityInterface;
use App\Repository\RepositoryInterface\CategoryRepositoryInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends AbstractRepository implements CategoryRepositoryInterface
{
    public const COLUMN_NAME = 'name';

    /**
     * SunCategoryParser constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @param int $pagination
     * @param int $id
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updatePagination(int $pagination, int $id): void
    {
        $category = $this->find($id);
        if ($category !== null) {
            $entityManager = $this->getEntityManager();
            $category->setPagination($pagination);
            $entityManager->persist($category);
            $entityManager->flush();
        }
    }

    /**
     * @param string $link
     * @param int $categoryId
     * @return void
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateLink(string $link, int $categoryId): void
    {
        $category = $this->find($categoryId);

        if ($category !== null) {
            $entityManager = $this->getEntityManager();
            $category->setLink($link);
            $entityManager->persist($category);
            $entityManager->flush();
        }
    }

    /**
     * @param int $startPage
     * @param int $lastPage
     * @param int $categoryId
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updatePageNumbers(int $startPage, int $lastPage, int $categoryId): void
    {
        $category = $this->find($categoryId);
        $lastValue = $lastPage === 0 ? null : $lastPage;

        if ($category !== null) {
            $entityManager = $this->getEntityManager();
            $category->setStartPageNumber($startPage);
            $category->setLastPageNumber($lastValue);
            $entityManager->persist($category);
            $entityManager->flush();
        }
    }

    /**
     * @param string $name
     * @return EntityInterface|null
     */
    public function findByName(string $name): ?EntityInterface
    {
        return $this->findOneBy([self::COLUMN_NAME => $name]);
    }
}
