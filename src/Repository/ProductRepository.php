<?php declare(strict_types=1);

namespace App\Repository;

use Generator;
use App\Entity\Product;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Repository\RepositoryInterface\ProductRepositoryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends AbstractRepository implements ProductRepositoryInterface
{
    /**
     * SunProductParser constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param Generator $entities
     * @param int $limit
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Generator $entities, int $limit = 100): void
    {
        $entityManager = $this->getEntityManager();
        $count = 0;

        foreach ($entities as $entity) {
            $count++;
            $entityManager->persist($entity);

            if ($count === $limit) {
                $entityManager->flush();
                $count = 0;
            }
        }
        $entityManager->flush();
    }
}
