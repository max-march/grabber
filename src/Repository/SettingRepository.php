<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Setting;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Setting|null find($id, $lockMode = null, $lockVersion = null)
 * @method Setting|null findOneBy(array $criteria, array $orderBy = null)
 * @method Setting[]    findAll()
 * @method Setting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SettingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Setting::class);
    }

    /**
     * @return Setting|null
     * @throws NonUniqueResultException
     */
    public function findFirst(): ?Setting
    {
        return $this->createQueryBuilder('s')->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Setting $setting
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Setting $setting): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($setting);
        $entityManager->flush();
    }
}
