<?php declare(strict_types=1);

namespace App\Repository\RepositoryInterface;

/**
 * Interface FileRepositoryInterface
 * @package App\Repository\RepositoryInterface
 */
interface FileRepositoryInterface extends RepositoryInterface
{
    /**
     * @param string $fileName
     * @return string|null
     */
    public function getFilePath(string $fileName): ?string;
}
