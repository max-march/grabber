<?php declare(strict_types=1);

namespace App\Repository\RepositoryInterface;

use App\Entity\EntityInterface;

/**
 * RepositoryInterface CategoryRepositoryInterface
 * @package App\Repository\RepositoryInterface
 */
interface CategoryRepositoryInterface extends RepositoryInterface
{
    /**
     * @param int $pagination
     * @param int $id
     */
    public function updatePagination(int $pagination, int $id): void;

    /**
     * @param string $link
     * @param int $categoryId
     */
    public function updateLink(string $link, int $categoryId): void;

    /**
     * @param int $startPage
     * @param int $lastPage
     * @param int $categoryId
     */
    public function updatePageNumbers(int $startPage, int $lastPage, int $categoryId): void;

    /**
     * @param string $category
     * @return EntityInterface|null
     */
    public function findByName(string $category): ?EntityInterface;
}
