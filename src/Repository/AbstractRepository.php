<?php declare(strict_types=1);

namespace App\Repository;

use App\Repository\RepositoryInterface\AbstractRepositoryInterface;
use App\Repository\RepositoryInterface\RepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Generator;
use PDO;

/**
 * Class AbstractRepository
 * @package App\Repository
 */
abstract class AbstractRepository extends ServiceEntityRepository implements AbstractRepositoryInterface
{
    private const QUEUE_COUNT_SQL = 'SELECT count(*) FROM enqueue WHERE queue = :queue';
    private const SQL_PARAM_QUEUE = 'queue';

    /**
     * @param Generator $entities
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Generator $entities): void
    {
        $entityManager = $this->getEntityManager();
        foreach ($entities as $entity) {
            $entityManager->persist($entity);
        }
        $entityManager->flush();
    }

    /**
     * @return void
     */
    public function deleteAll(): void
    {
        $this->createQueryBuilder('e')->delete()->getQuery()->execute();
    }

    /**
     * @param string $site
     * @return void
     */
    public function deleteAllBySite(string $site): void
    {
        $this->createQueryBuilder('e')
            ->where('e.siteName = :site')
            ->setParameter('site', $site)
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * @param int $id
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function delete(int $id): void
    {
        $entity = $this->find($id);
        if ($entity !== null) {
            $entityManager = $this->getEntityManager();
            $entityManager->remove($entity);
            $entityManager->flush();
        }
    }

    /**
     * @param array $array
     * @return array
     */
    public function findByIds(array $array): array
    {
        return $this->findBy([RepositoryInterface::COLUMN_ID => $array]);
    }


    /**
     * @param string $queueName
     * @return int
     * @throws DBALException
     */
    public function fetchQueueCount(string $queueName): int
    {
        $entityManager = $this->getEntityManager();
        $statement = $entityManager->getConnection()->prepare(self::QUEUE_COUNT_SQL);
        $statement->bindValue(self::SQL_PARAM_QUEUE, $queueName);
        $statement->execute();

        return (int)$statement->fetch(PDO::FETCH_COLUMN);
    }
}
