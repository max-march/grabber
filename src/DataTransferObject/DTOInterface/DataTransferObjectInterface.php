<?php declare(strict_types=1);

namespace App\DataTransferObject\DTOInterface;

/**
 * Interface DataTransferObjectInterface
 * @package App\DataTransferObject
 */
interface DataTransferObjectInterface
{
    /**
     * @return string
     */
    public function getName(): string;


    /**
     * @return string
     */
    public function getLink(): ?string;
}
