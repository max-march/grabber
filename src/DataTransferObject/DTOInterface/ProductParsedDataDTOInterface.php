<?php declare(strict_types=1);

namespace App\DataTransferObject\DTOInterface;

/**
 * Interface ProductParsedDataDTOInterface
 * @package App\DataTransferObject\DTOInterface
 */
interface ProductParsedDataDTOInterface
{

    /**
     * @return string
     */
    public function getVendor(): string;

    /**
     * @param string $vendor
     * @return ProductParsedDataDTOInterface
     */
    public function setVendor(string $vendor): ProductParsedDataDTOInterface;

    /**
     * @return string|null
     */
    public function getVendorCode(): ?string;

    /**
     * @param null|string $vendorCode
     * @return ProductParsedDataDTOInterface
     */
    public function setVendorCode(?string $vendorCode): ProductParsedDataDTOInterface;

    /**
     * @return null|string
     */
    public function getDescription(): ?string;

    /**
     * @param null|string $description
     * @return ProductParsedDataDTOInterface
     */
    public function setDescription(?string $description): ProductParsedDataDTOInterface;

    /**
     * @return null|string
     */
    public function getKeyWords(): ?string;

    /**
     * @param null|string $keyWords
     * @return ProductParsedDataDTOInterface
     */
    public function setKeyWords(?string $keyWords): ProductParsedDataDTOInterface;

    /**
     * @return null|string
     */
    public function getPositionName(): ?string;

    /**
     * @param null|string $positionName
     * @return ProductParsedDataDTOInterface
     */
    public function setPositionName(?string $positionName): ProductParsedDataDTOInterface;

    /**
     * @return null|int
     */
    public function getPrice(): ?int;

    /**
     * @param null|int $price
     * @return ProductParsedDataDTOInterface
     */
    public function setPrice(?int $price): ProductParsedDataDTOInterface;

    /**
     * @return null|string
     */
    public function getImages(): ?string;

    /**
     * @param null|string $images
     * @return ProductParsedDataDTOInterface
     */
    public function setImages(string $images): ProductParsedDataDTOInterface;

    /**
     * @return int|null
     */
    public function getUniqueId(): ?int;

    /**
     * @param null|int $uniqueId
     * @return ProductParsedDataDTOInterface
     */
    public function setUniqueId(?int $uniqueId): ProductParsedDataDTOInterface;
}
