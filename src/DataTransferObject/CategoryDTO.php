<?php declare(strict_types=1);

namespace App\DataTransferObject;

use App\DataTransferObject\DTOInterface\CategoryDTOInterface;

/**
 * Class CategoryDTO
 * @package App\DataTransferObject
 */
class CategoryDTO implements CategoryDTOInterface
{
    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $link
     */
    private $link;

    /**
     * @var string $siteUrl
     */
    private $siteUrl;

    /**
     * @var int
     */
    private $pagination;

    /**
     * @var string
     */
    private $paginationPattern;

    /**
     * CategoryDTO constructor.
     * @param string $name
     * @param string|null $link
     * @param string $siteUrl
     * @param int|null $pagination
     * @param string|null $paginationPatten
     */
    public function __construct(
        string $name,
        ?string $link,
        string $siteUrl,
        ?int $pagination,
        ?string $paginationPatten = null
    ) {
        $this->name = $name;
        $this->link = $link;
        $this->siteUrl = $siteUrl;
        $this->pagination = $pagination;
        $this->paginationPattern = $paginationPatten;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }


    /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }


    /**
     * @return string
     */
    public function getSiteUrl(): string
    {
        return $this->siteUrl;
    }

    /**
     * @return int
     */
    public function getPagination(): ?int
    {
        return $this->pagination;
    }

    /**
     * @param int|null $pagination
     * @return CategoryDTO
     */
    public function setPagination(?int $pagination): CategoryDTO
    {
        $this->pagination = $pagination;

        return $this;
    }

    /**
     * @return string
     */
    public function getLinkPattern(): ?string
    {
        return $this->paginationPattern;
    }

    /**
     * @param string|null $paginationPattern
     * @return CategoryDTO
     */
    public function setLinkPattern(?string $paginationPattern): CategoryDTO
    {
        $this->paginationPattern = $paginationPattern;

        return $this;
    }
}
