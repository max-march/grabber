<?php declare(strict_types=1);

namespace App\Factory;

use App\DataTransferObject\CategoryDTO;
use App\DataTransferObject\DTOInterface\CategoryDTOInterface;

/**
 * Class CategoryDTOFactory
 * @package App\Factory
 */
class CategoryDTOFactory
{
    /**
     * @param string $name
     * @param string|null $link
     * @param string $siteUrl
     * @param int|null $pagination
     * @return CategoryDTOInterface
     */
    public function create(string $name, ?string $link, string $siteUrl, ?int $pagination = null): CategoryDTOInterface
    {
        return new CategoryDTO($name, $link, $siteUrl, $pagination);
    }
}
