<?php declare(strict_types=1);

namespace App\Factory;

use App\Entity\Product;

/**
 * Class ProductFactory
 * @package App\Factory
 */
class ProductFactory
{
    /**
     * @return Product
     */
    public function create(): Product
    {
        return new Product();
    }
}
