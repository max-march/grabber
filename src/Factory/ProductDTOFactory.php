<?php declare(strict_types=1);

namespace App\Factory;

use App\DataTransferObject\DTOInterface\ProductDTOInterface;
use App\DataTransferObject\ProductDTO;
use App\Entity\EntityInterface;

/**
 * Class ProductDTOFactory
 * @package App\Factory
 */
class ProductDTOFactory
{
    /**
     * @param string $name
     * @param string $link
     * @param string $image
     * @param EntityInterface $category
     * @return ProductDTO
     */
    public function create(string $name, string $link, string $image, EntityInterface $category): ProductDTOInterface
    {
        return new ProductDTO($name, $link, $image, $category);
    }
}
