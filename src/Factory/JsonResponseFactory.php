<?php declare(strict_types=1);

namespace App\Factory;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class JsonResponseFactory
 * @package App\Factory
 */
class JsonResponseFactory
{
    /**
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    public function create(array $data, int $code): JsonResponse
    {
        return new JsonResponse($data, $code);
    }
}
