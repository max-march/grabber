<?php declare(strict_types=1);

namespace App\Factory;

use App\DataTransferObject\DTOInterface\ProductParsedDataDTOInterface;
use App\DataTransferObject\ProductParsedDataDTO;

/**
 * Class ProductParsedDataFactory
 * @package App\Factory
 */
class ProductParsedDataFactory
{
    /**
     * @return ProductParsedDataDTOInterface
     */
    public function create(): ProductParsedDataDTOInterface
    {
        return new ProductParsedDataDTO();
    }
}
