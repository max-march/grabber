<?php

namespace App\Command;

use App\Controller\AbstractController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use App\Queues\QueuesInterface\SunConsumerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Queues\QueuesInterface\DiashaConsumerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class GenerateReportCommand
 * @package App\Command
 */
class GenerateReportCommand extends Command
{
    private const COMMAND = 'make:report';
    private const DESCRIPTION = 'Generate products excel file';
    private const HELP = 'This command allows you to generate products excel file';
    private const OUTPUT_DESCRIPTION = 'Products excel file generator';
    private const OUTPUT_SPACE = '============';
    private const OUTPUT_EMPTY_VALUE = '';
    private const SCRIPT_WORKING_TIME_PATTERN = 'Script working time %s sec';

    private const SITE_ARGUMENT = 'site';

    /**
     * @var array $consumers
     */
    private $consumers;

    /**
     * GenerateReportCommand constructor.
     * @param SunConsumerInterface $sunLineConsumer
     * @param DiashaConsumerInterface $diashaConsumer
     * @param ParameterBagInterface $params
     * @param string|null $name
     */
    public function __construct(
        SunConsumerInterface $sunLineConsumer,
        DiashaConsumerInterface $diashaConsumer,
        ParameterBagInterface $params,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->consumers = [
            $params->get(AbstractController::SUN_SITE_PARAM) => $sunLineConsumer,
            $params->get(AbstractController::DIASHA_SITE_PARAM) => $diashaConsumer
        ];
    }

    /**
     * @var string $defaultName
     */
    protected static $defaultName = self::COMMAND;

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setDescription(self::DESCRIPTION)->setHelp(self::HELP)
            ->addArgument(self::SITE_ARGUMENT, InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln([self::OUTPUT_DESCRIPTION, self::OUTPUT_SPACE, self::OUTPUT_EMPTY_VALUE,]);
        $start = microtime(true);

        $site = $input->getArgument(self::SITE_ARGUMENT);
        $this->consumers[$site]->startProcessing($site);

        $output->writeln(
            sprintf(self::SCRIPT_WORKING_TIME_PATTERN, round(microtime(true) - $start, 4))
        );
    }
}
