$('.pmd-switch').on('click', '.email-send', function () {
    let checkedValue = $(".email-send").is(":checked");

    $.ajax(
        {
            url: '/admin/settings/email-send/update',
            method: 'PATCH',
            dataType: "JSON",
            data: {
                "emailSend": checkedValue,
            },
            success: function (data) {
                if (data.success === true) {
                    $.notify("Setting updated", {
                        color: "#fff",
                        background: "#468847",
                        align:"right",
                        verticalAlign:"top",
                        animationType:"drop"
                    });
                }
            },
        }
    );
});

$(document).ready(function () {
    $.ajax(
        {
            url: '/admin/settings/email-send',
            method: 'GET',
            dataType: "JSON",
            success: function (data) {
                if (data.success === true) {
                    let checkedValue = data.payload[0];
                    $(".email-send").attr("checked", checkedValue);
                }
            },
        }
    );
});